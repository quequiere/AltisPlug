package com.altisplug;

import java.util.logging.Logger;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.altisplug.commandes.CommandeHandler;
import com.altisplug.listener.PlayerListener;
import com.earth2me.essentials.Essentials;
import com.earth2me.essentials.IEssentials;

public class AltisPlug extends JavaPlugin
{
	public final static Logger log = Logger.getLogger("Minecraft");
	private static Plugin plugin;

	public static Permission permission = null;
	public static Economy economy = null;
	public static Essentials essentials = null;

	@Override
	public void onDisable()
	{
		log.info("AltisPlug disabled.");
	}

	@Override
	public void onEnable()
	{
		plugin = this;

		Config.Init(this.getConfig(), this);
		getServer().getPluginManager().registerEvents(new PlayerListener(getServer()), this);
		getCommand("altis").setExecutor(new CommandeHandler());

	

	}

	public boolean setupEconomy()
	{
		RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null)
		{
			economy = economyProvider.getProvider();
		}

		return (economy != null);
	}

	public boolean setupPermissions()
	{
		RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);

		if (permissionProvider != null)
		{
			permission = permissionProvider.getProvider();
		}

		return (permission != null);
	}

	public static Plugin getPlugin()
	{
		return plugin;
	}

}
