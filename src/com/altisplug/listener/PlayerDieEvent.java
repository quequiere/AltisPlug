package com.altisplug.listener;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.altisplug.Config;
import com.altisplug.Infraction.EnumDelis;
import com.altisplug.Infraction.Infraction;
import com.altisplug.classes.superClasse.Joueur;
import com.altisplug.classes.superClasse.JoueurPunissable;
import com.altisplug.hopital.Hopital;

public class PlayerDieEvent
{
	
	public static void dieEvent(PlayerDeathEvent e, Joueur j, Player p)
	{
		//A coder: Requete d'ambulancier
		
		if(j instanceof JoueurPunissable)
		{
			JoueurPunissable jp = (JoueurPunissable) j;
			
			//teleportation vers lhopital
			
			p.sendMessage(ChatColor.BLUE+"Vous avez choisi d'être réanimé à l'hopital.");
			p.sendMessage(ChatColor.BLUE+"Vous devrez donc payer la somme de "+ChatColor.RED+Config.hospitalCost+" $");
			p.sendMessage(ChatColor.BLUE+"Celle ci se trouve dans les ammende a payer ! Vous avez "+ChatColor.RED+Config.hospitalTimeToPay+" minutes");
			p.sendMessage(ChatColor.BLUE+"pour payer votre dette.");
			
			Infraction i = new Infraction(EnumDelis.facturehopital);
			i.setConstrol("Serveur", Config.hospitalCost, Config.hospitalTimeToPay);
			jp.addInfraction(i);
			
			Hopital.autoTeleport(p);
		}
	}

}
