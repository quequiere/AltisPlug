package com.altisplug.commandes;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.altisplug.AltisPlug;
import com.altisplug.Infraction.EnumDelis;
import com.altisplug.Infraction.Infraction;
import com.altisplug.classes.superClasse.EnumClasses;
import com.altisplug.classes.superClasse.Joueur;
import com.altisplug.classes.superClasse.JoueurPunissable;

public class InfractionCommande
{
	public static void execute(CommandSender sender, String[] args)
	{
		if (args.length < 3)
		{
			sender.sendMessage(ChatColor.RED + "/alt infraction joueur delis");
			return;
		}
		else
		{
			EnumDelis delis = EnumDelis.getValue(args[2]);

			if (delis == null)
			{
				sender.sendMessage(ChatColor.RED + "Ce delis n'existe pas!");
				for (EnumDelis d : EnumDelis.values())
				{
					sender.sendMessage(ChatColor.RED + "-" + d.toString());
				}
				return;
			}

			Joueur j = Joueur.getJoueur(args[1]);
			if (j != null)
			{
				if(j instanceof JoueurPunissable)
				{
					JoueurPunissable jp = (JoueurPunissable) j;
					Infraction i = new Infraction(delis);
					jp.addInfraction(i);
					
					sender.sendMessage(ChatColor.GREEN + "Incraftion manuelle ajoutee !");
				}
				else
				{
					sender.sendMessage(ChatColor.RED + "Ceci n'est pas un JoueurPunissable.");
				}
			}
			else
			{
				sender.sendMessage(ChatColor.RED + "Joueur non chargé!");
			}

		}

	}
}
