package com.altisplug.Infraction;

public enum EnumDelis
{

	//ces temps tiennent en compte du temps de connection, pas du temps absolu

	facturehopital(0,120,9),
	insultesChat(1,20,3),
	infligerBlessure(1,20,3),
	provocation(1,20,3),
	spamChat(1,20,3),
	vitesse(2,30,1),
	racket(3,40,3),
	braquageBanque(4,120,1),
	assassinatCivil(4,60,3),
	assassinatPolice(5,80,3),
	assassinatRebel(3,30,3),
	refusObtenperer(4,40,1);
	
	private int etoile;
	private int minuteAvantSupression;
	private int maxCumul;
	
	private EnumDelis(int etoile, int minuteAvantSupression, int maxCumul)
	{
		this.etoile = etoile;
		this.minuteAvantSupression = minuteAvantSupression;
		this.maxCumul=maxCumul;
	}
	
	public int getEtoile()
	{
		return this.etoile;
	}
	
	public int getminuteAvantSupression()
	{
		return this.minuteAvantSupression;
	}
	
	public static EnumDelis getValue(String s)
	{
		for(EnumDelis d:EnumDelis.values())
		{
			if(d.toString().equalsIgnoreCase(s))
			{
				return d;
			}
		}
		
		return null;
	}
	
	
}
