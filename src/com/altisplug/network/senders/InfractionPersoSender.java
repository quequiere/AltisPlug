package com.altisplug.network.senders;

import java.io.PrintWriter;

import com.altisplug.Infraction.Infraction;
import com.altisplug.classes.superClasse.Joueur;
import com.altisplug.classes.superClasse.JoueurPunissable;

public class InfractionPersoSender
{
	public static void go(PrintWriter out, String target)
	{
		
		Joueur j = Joueur.getJoueur(target);
		if(j==null||!(j instanceof JoueurPunissable))
		{
			out.println(false);
		}
		else
		{
			JoueurPunissable jp = (JoueurPunissable) j;
			
			String allInf = "";
			int x = 10;
			
			for(Infraction i:jp.getAllInfraction())
			{
				i.id=x;
				allInf = allInf+"#"+i.toJson();
				x++;
			}
			out.println(allInf);
		}
	}

}
