package com.altisplug.network.senders;

import java.io.PrintWriter;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.altisplug.Infraction.Infraction;
import com.altisplug.classes.superClasse.Joueur;
import com.altisplug.classes.superClasse.JoueurPunissable;

public class DataPersoSender
{
	public static void go(PrintWriter out, String target)
	{
		
		if(target.equalsIgnoreCase("_All_"))
		{
			out.println(Bukkit.getOnlinePlayers().size());
			for(Player p:Bukkit.getOnlinePlayers())
			{
				Joueur j = Joueur.getJoueur(p.getName());
				if(j==null)
				{
					System.out.println("non trouve");
					out.println(false);
				}
				else
				{
					if(j instanceof JoueurPunissable)
					{
						JoueurPunissable jp = (JoueurPunissable) j ;
						
					//	if(jp.getStar()>0)
						//{
						
							out.println(p.getName());
							
							out.println(j.getClasse().toString());
							
							out.println(p.getLocation().getX()+"#"+p.getLocation().getY()+"#"+p.getLocation().getZ());
							out.println(jp.getStar());
						//}
					
				
					}
					
				}
			}
			
		}
		else
		{

			Joueur j = Joueur.getJoueur(target);
			if(j==null)
			{
				out.println(false);
			}
			else
			{
				out.println(j.getClasse().toString());
				
				if(j instanceof JoueurPunissable)
				{
					JoueurPunissable jp = (JoueurPunissable) j;
					out.println(jp.getStar());
				}
			}
		}
		
		
	}

}
