package com.altisplug.Infraction;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.altisplug.AltisPlug;
import com.altisplug.classes.superClasse.Joueur;
import com.altisplug.classes.superClasse.JoueurPunissable;
import com.earth2me.essentials.User;

public class AutoDisapearStarThread extends Thread
{

	public AutoDisapearStarThread()
	{

	}

	public void run()
	{
		long last = 0;

		while (true)
		{
			long now = System.currentTimeMillis() / 1000;
			if (last != now)
			{
				last = now;

				for (Player p : Bukkit.getOnlinePlayers())
				{
					Joueur j = Joueur.getJoueur(p.getName());

					if (j instanceof JoueurPunissable)
					{
						JoueurPunissable jp = (JoueurPunissable) j;
						
						boolean unPayed = false;

						for (Infraction i : jp.getActiveAndNotControl())
						{	
							long diff = (i.getDateDeLinfraction()+(i.getDelis().getminuteAvantSupression()*60))-(System.currentTimeMillis()/1000);
							
							if(diff<0)
							{
								p.sendMessage(ChatColor.GREEN+"Un avis de recherche est arrivé à expiration!");
								i.setActivated(false);
							}
						}
					}
				}

			}

		}
	}
}
