package com.altisplug.Infraction;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.altisplug.AltisPlug;
import com.altisplug.classes.superClasse.Joueur;
import com.altisplug.classes.superClasse.JoueurPunissable;
import com.earth2me.essentials.User;

public class LimitePayDateThread extends Thread
{

	public LimitePayDateThread()
	{

	}

	public void run()
	{
		long last = 0;

		while (true)
		{
			long now = System.currentTimeMillis() / 1000;
			if (last != now)
			{
				last = now;

				for (Player p : Bukkit.getOnlinePlayers())
				{
					Joueur j = Joueur.getJoueur(p.getName());

					if (j instanceof JoueurPunissable)
					{
						JoueurPunissable jp = (JoueurPunissable) j;
						
						boolean unPayed = false;

						for (Infraction i : jp.getActiveAndNotPayInfraction())
						{	
							i.setRemaningSeconds(i.getRemaningSeconds() - 1);

							if (i.getRemaningSeconds() < 1)
							{
								i.setTimedOut();
								unPayed = true;
							}

						}
						User u = AltisPlug.essentials.getUser(p);
						String jailName="";
						try
						{
							jailName = (String) AltisPlug.essentials.getJails().getList().toArray()[0];
						}
						catch (Exception e1)
						{
							e1.printStackTrace();
						}

						if (unPayed)
						{
							if (u.isJailed())
							{
								p.sendMessage(ChatColor.RED + "Nouvelle amende impayée !!!");
								
								p.sendMessage(ChatColor.RED + "\n----Recapitulatif----");
								p.sendMessage(ChatColor.RED + "Une amende est arrivée à terme sans que vous ne l'ayiez payée");
								p.sendMessage(ChatColor.RED + "Nous vous transferons à la prison la plus proche");
								p.sendMessage(ChatColor.RED + "Vous devrez attendre le controle par un policier qui décidera");
								p.sendMessage(ChatColor.RED + "combien de temps vous aller passer en prison pour payer votre dette.");
							}
							else
							{
								try
								{
									AltisPlug.essentials.getJails().sendToJail(u, jailName);
									u.setJailed(true);
									u.setJail(jailName);
								}
								catch (Exception e)
								{
									e.printStackTrace();
								}
								p.sendMessage(ChatColor.RED + "Une amende est arrivée à terme sans que vous ne l'ayiez payée");
								p.sendMessage(ChatColor.RED + "Nous vous transferons à la prison la plus proche");
								p.sendMessage(ChatColor.RED + "Vous devrez attendre le controle par un policier qui décidera");
								p.sendMessage(ChatColor.RED + "combien de temps vous aller passer en prison pour payer votre dette.");

							}

						}
					}
				}

			}

		}
	}
}
