package com.altisplug.commandes;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.altisplug.AltisPlug;
import com.altisplug.classes.superClasse.EnumClasses;
import com.altisplug.classes.superClasse.Joueur;

public class ClasseCommande
{
	public static void execute(CommandSender sender, String[] args)
	{
		if (args.length < 2)
		{
			sender.sendMessage(ChatColor.RED + "/alt classe [policier/Rebelle]");
			return;
		}
		else
		{
			EnumClasses classe = EnumClasses.getClasseFromString(args[1]);

			if (classe == null)
			{
				sender.sendMessage(ChatColor.RED + "Cette classe n'existe pas ! Policier/Rebelle");
				return;
			}

			if (sender instanceof Player)
			{
				Player p = (Player) sender;
				Joueur j = Joueur.getJoueur(p.getName());
				
				if(!j.isClasseNul()&&args.length!=3)
				{
					sender.sendMessage(ChatColor.RED + "Vous n'avez pas le droit de changer de classe!");
				}
				else if (args.length == 2)
				{
					j.setClasse(classe);

					sender.sendMessage(ChatColor.BLUE + "Vous rejoignez la classe: " + classe.toString());
				}
				else if (args.length == 3)
				{
					if(AltisPlug.permission.has(sender, "altisplug.commande.classeAdmin"))
					{
						Joueur.getJoueur(args[2]).setClasse(classe);
						sender.sendMessage(ChatColor.BLUE + args[2] + "rejoins la classe: " + classe.toString());
					}
					else
					{
						sender.sendMessage(ChatColor.RED + "Vous devez avoir la perm: altisplug.commande.classeAdmin");
					}
				}
			}
			else
			{
				//si la commande viens de la console
				
				if(args.length!=3)
				{
					sender.sendMessage(ChatColor.RED + "/altis classe policier player");
				}
				else
				{
					Joueur.getJoueur(args[2]).setClasse(classe);
					sender.sendMessage(ChatColor.BLUE + args[2] + "rejoins la classe: " + classe.toString());
				}
				
			}

		}

	}
}
