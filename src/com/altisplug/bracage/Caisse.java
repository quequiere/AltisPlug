package com.altisplug.bracage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import com.altisplug.AltisPlug;
import com.altisplug.Config;
import com.altisplug.util.qqcolor;

public class Caisse
{
	public static int totalTimeToBraquageSeconds = 80;
	
	private Location loc;
	private int currentMoney=0;
	private int MoneyMax=800;
	
	private static ArrayList<Caisse> caisseList = new ArrayList<Caisse>();
	
	public Caisse(Location l)
	{
		this.loc=l;
		caisseList.add(this);
		Caisse.save();
	}
	
	public Caisse(Location l,int max,int current)
	{
		this.loc=l;
		this.MoneyMax=max;
		this.currentMoney=current;
		caisseList.add(this);
	}
	
	public Location getLocation()
	{
		return this.loc;
	}
	
	public static Caisse getNearestCaisse(Location l)
	{
		double bestDist = 999999999;
		Caisse toret = null;
		
		for(Caisse c:caisseList)
		{
			double curDist = c.getLocation().distance(l);
			if(curDist<bestDist)
			{
				bestDist=curDist;
				toret=c;
			}
		}
		
		if(bestDist>5)
		{
			return null;
		}
		
		return toret;
	}
	
	public double getFrequence()
	{
		double freq = this.MoneyMax/totalTimeToBraquageSeconds;
		
		return freq;
	}

	public int getCurrentMoney()
	{
		return currentMoney;
	}

	public void setCurrentMoney(int currentMoney)
	{
		this.currentMoney = currentMoney;
		Caisse.save();
	}

	public int getMoneyMax()
	{
		return MoneyMax;
	}

	public void setMoneyMax(int moneyMax)
	{
		MoneyMax = moneyMax;
		Caisse.save();
	}
	
	
	public static void loadAll(List<String> e)
	{
		for(String s:e)
		{
			String[] coupe =s.split("#");
			World w = Bukkit.getWorld(coupe[0]);
			Location l = new Location(w,Double.parseDouble(coupe[1]),Double.parseDouble(coupe[2]),Double.parseDouble(coupe[3]));
			int max = Integer.parseInt(coupe[4]);
			int current = Integer.parseInt(coupe[5]);
			
			new Caisse(l,max,current);
			
			System.out.println(qqcolor.ANSI_CYAN+"Adding Caisse : "+s+qqcolor.ANSI_RESET);
		}
	}
	
	public static void save()
	{
		ArrayList<String> liste = new ArrayList<String>();
		
		for(Caisse c:caisseList)
		{
			Location tloc = c.getLocation();
			String s=  tloc.getWorld().getName()+"#"+tloc.getBlockX()+"#"+tloc.getBlockY()+"#"+tloc.getBlockZ()+"#"+c.getMoneyMax()+"#"+c.getCurrentMoney();
			liste.add(s);
		}
		
		Config.config.set("play.caisse.list", liste);
		
		try
		{
			Config.config.save(AltisPlug.getPlugin().getDataFolder() + File.separator + "config.yml");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	

}
