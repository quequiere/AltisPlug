package com.altisplug.Infraction;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class Infraction
{
	private boolean activated = true;
	private long dateDeLinfraction;


	private EnumDelis delis;
	
	private boolean hasBeenControled = false;
	private String controledBy;
	private double toPay;
	private long secondesRestantes;
	
	private boolean hasBeenPayed = false;
	
	public int id=-1;
	
	public Infraction(EnumDelis delis)
	{
		this.dateDeLinfraction = System.currentTimeMillis()/1000;
		this.delis = delis;
	}
	
	public void setConstrol(String player,double toPay,int minutesToPay)
	{
		this.controledBy=player;
		this.hasBeenControled=true;
		this.toPay=toPay;
		this.secondesRestantes=minutesToPay*60;
	}
	
	public void close(boolean payed)
	{
		this.activated=false;
		this.secondesRestantes=0;
		this.hasBeenPayed=payed;
	}
	
	public boolean isActiveAndNotPay()
	{
		if(this.activated&&!this.hasBeenPayed)
		{
			return true;
		}
		return false;
	}
	
	public boolean isActiveAndNotControl()
	{
		if(this.activated&&!this.hasBeenControled)
		{
			return true;
		}
		return false;
	}
	
	public EnumDelis getDelis()
	{
		return this.delis;
	}
	
	public void setTimedOut()
	{
		this.activated=false;
		this.hasBeenPayed=false;
	}
	
	public long getRemaningSeconds()
	{
		return this.secondesRestantes;
	}
	
	public void setRemaningSeconds(long l)
	{
		this.secondesRestantes=l;
	}
	
	
	public boolean hasBeenControl()
	{
		return this.hasBeenControled;
	}
	
	
	public String toJson()
	{
		Gson gson = new GsonBuilder().create();
		String object = gson.toJson(this);
		return object;
	}
	
	public static Infraction fromJson(String s)
	{
		Gson gson = new GsonBuilder().create();
		Infraction object = gson.fromJson(s, Infraction.class);
		return object;
	}

	public long getDateDeLinfraction()
	{
		return dateDeLinfraction;
	}

	public void setActivated(boolean activated)
	{
		this.activated = activated;
	}

	public double getToPay()
	{
		return toPay;
	}
	

	

}
