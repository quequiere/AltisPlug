package com.altisplug.commandes;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.altisplug.hopital.Hopital;

public class addHopital
{
	public static void execute(CommandSender sender, String[] args)
	{
		if (sender instanceof Player)
		{
			Player p = (Player) sender;
			Hopital.addNewHop(p.getLocation());
			sender.sendMessage(ChatColor.GREEN + "Création en terminée");
		}
		else
		{
			sender.sendMessage(ChatColor.RED + "Impossible de faire cela depuis la console !");
		}
		return;

	}
}
