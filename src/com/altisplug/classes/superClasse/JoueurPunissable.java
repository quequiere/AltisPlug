package com.altisplug.classes.superClasse;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.OfflinePlayer;

import com.altisplug.Infraction.Infraction;

public class JoueurPunissable extends Joueur
{
	
	private ArrayList<Infraction> infractionListe;
	private long lastFreeze = 0;

	public JoueurPunissable(UUID id, OfflinePlayer player)
	{
		super(id, player);
		infractionListe= new ArrayList<Infraction>();
	}
	
	public void addInfraction(Infraction i)
	{
		infractionListe.add(i);
	}
	
	public int getStar()
	{
		int tot = 0;
		for(Infraction i:this.infractionListe)
		{
			if(i.isActiveAndNotControl())
			{
				tot += i.getDelis().getEtoile();
			}
		}
		
		return tot;
	}
	
	public  ArrayList<Infraction> getAllInfraction()
	{
		return infractionListe;
	}
	
	public boolean isFreeze()
	{
		long now = System.currentTimeMillis()/1000;
		long diff = now - this.lastFreeze;
		
		if(diff>8)
		{
			return false;
		}
		
		return true;
	}
	
	public void setFreeze()
	{
		this.lastFreeze= System.currentTimeMillis()/1000;
	}
	
	public  ArrayList<Infraction> getActiveAndNotPayInfraction()
	{
		 ArrayList<Infraction> itemp = new ArrayList<Infraction>();
		 for(Infraction i:this.infractionListe)
		 {
			 if(i.isActiveAndNotPay()&&i.hasBeenControl())
			 {
				 itemp.add(i);
			 }
		 }
		 
		 return itemp;
	}
	
	public  ArrayList<Infraction> getActiveAndNotControl()
	{
		 ArrayList<Infraction> itemp = new ArrayList<Infraction>();
		 for(Infraction i:this.infractionListe)
		 {
			 if(i.isActiveAndNotPay()&&!i.hasBeenControl())
			 {
				 itemp.add(i);
			 }
		 }
		 
		 return itemp;
	}
	
	public Infraction getInfractionById (int id)
	{
		
		for(Infraction i : this.infractionListe)
		{
			if(i.id==id)
			{
				return i;
			}
		}
		
		return null;
	}

}
