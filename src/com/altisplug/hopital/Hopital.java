package com.altisplug.hopital;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.altisplug.AltisPlug;
import com.altisplug.Config;
import com.altisplug.util.qqcolor;

public class Hopital
{
	private Location loc;
	protected static ArrayList<Hopital> hopitaux = new ArrayList<Hopital>();

	public Hopital(Location loc)
	{
		this.loc = loc;
		hopitaux.add(this);
	}

	public Location getLoc()
	{
		return loc;
	}
	
	public static void loadAll(List<String> e)
	{
		for(String s:e)
		{
			String[] coupe =s.split("#");
			World w = Bukkit.getWorld(coupe[0]);
			Location l = new Location(w,Double.parseDouble(coupe[1]),Double.parseDouble(coupe[2]),Double.parseDouble(coupe[3]));
			new Hopital(l);
			
			System.out.println(qqcolor.ANSI_YELLOW+"Adding hospital : "+s+qqcolor.ANSI_RESET);
		}
	}
	
	public static void addNewHop(Location l)
	{
		new Hopital(l);
		
		ArrayList<String> liste = new ArrayList<String>();
		
		for(Hopital hp : hopitaux)
		{
			Location tloc = hp.getLoc();
			String s = tloc.getWorld().getName()+"#"+tloc.getBlockX()+"#"+tloc.getBlockY()+"#"+tloc.getBlockZ();
			liste.add(s);
		}
		
		Config.config.set("play.hospital.list", liste);
		try
		{
			Config.config.save(AltisPlug.getPlugin().getDataFolder() + File.separator + "config.yml");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static Hopital getNearest(Location l)
	{
		double old = 99999999;
		Hopital curhp =null;
		
		for(Hopital hp:hopitaux)
		{
			double current = hp.getLoc().distance(l);
			if(current<old)
			{
				curhp=hp;
			}
		}
		
		return curhp;
	}
	
	public static void autoTeleport(Player p)
	{
		getNearest(p.getLocation()).teleportPlayer(p);
	}
	
	public void teleportPlayer(Player p)
	{
		p.setBedSpawnLocation(this.loc);
	}

}
