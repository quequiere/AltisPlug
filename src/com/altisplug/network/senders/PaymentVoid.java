package com.altisplug.network.senders;

import java.io.PrintWriter;

import org.bukkit.ChatColor;

import com.altisplug.Infraction.Infraction;
import com.altisplug.classes.superClasse.Joueur;
import com.altisplug.classes.superClasse.JoueurPunissable;

public class PaymentVoid
{
	public static void go(PrintWriter out, String target, int id)
	{
		Joueur j = Joueur.getJoueur(target);
		
		
		if(j!=null&&j instanceof JoueurPunissable)
		{
			JoueurPunissable jp = (JoueurPunissable) j;
			Infraction i = jp.getAllInfraction().get(id-10);
			
			if(jp.moneyGet()>=i.getToPay())
			{
				jp.getPlayer().sendMessage(ChatColor.BLUE+"Félicitation, votre infraction est payée !");
				jp.moneyTake(i.getToPay());
				i.close(true);
			}
			else
			{
				jp.getPlayer().sendMessage(ChatColor.RED+"Vous n'avez pas assez d'argent pour payer cette infraction!");
			}
		}
	}

}
