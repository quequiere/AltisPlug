package com.altisplug.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.altisplug.network.senders.DataPersoSender;
import com.altisplug.network.senders.InfractionPersoSender;
import com.altisplug.network.senders.OpenDossier;
import com.altisplug.network.senders.PaymentVoid;
import com.altisplug.network.senders.PvSender;


public class SocketHandler implements Runnable
{

	private Socket sock;

	SocketHandler(Socket sock)
	{
		this.sock = sock;
	}

	public void run()
	{

		try
		{

			String remoteAddress = sock.getInetAddress().getHostAddress();

			BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			PrintWriter out = new PrintWriter(new OutputStreamWriter(sock.getOutputStream()));

			String entete = in.readLine();
			
			if(entete.equals("Requete Infraction"))
			{				
				String target = in.readLine();
				InfractionPersoSender.go(out,target);
			}
			else if(entete.equals("Requete Update Joueur"))
			{		
				String target = in.readLine();
				DataPersoSender.go(out,target);
			}
			else if(entete.equals("Requete Payment"))
			{		
				String target = in.readLine();
				int id= Integer.parseInt(in.readLine());
				PaymentVoid.go(out,target,id);
			}
			else if(entete.equals("Requete dossier"))
			{		
				String target = in.readLine();
				OpenDossier.go(out,target);
			}
			else if(entete.equals("Envoi PV"))
			{		
				String target = in.readLine();
				PvSender.go(out,in,target);
			}
			
			
			out.flush();
			out.close();


		}
		catch (IOException ioe)
		{
			ioe.printStackTrace();
		}
		
	}

}
