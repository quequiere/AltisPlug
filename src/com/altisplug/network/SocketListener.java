package com.altisplug.network;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketListener implements Runnable
{

	private static int port = 0;
	private static boolean running;
	private static ServerSocket listener = null;
	private static Socket client = null;

	public SocketListener(int Port)
	{
		port = Port;
	}

	public void shutdown()
	{
		running = false;
		try
		{
			listener.close();
		}
		catch (IOException ioe)
		{
		}
	}

	@Override
	public void run()
	{
		System.out.println("Demarage du SocketHandler pour les clients");
		running = true;
		try
		{

			if (listener == null)
			{
				listener = new ServerSocket(port);
			}

			while (running)
			{
				if (listener != null && !listener.isClosed())
				{
					client = listener.accept();
					SocketHandler conn = new SocketHandler(client);
					Thread t = new Thread(conn);
					t.start();
				}
				else
				{
					running = false;
				}

			}

		}
		catch (IOException ioe)
		{

		}
	}
}