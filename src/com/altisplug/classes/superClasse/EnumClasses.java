package com.altisplug.classes.superClasse;

public enum EnumClasses
{
	Policier,
	Rebelle,
	Civil,
	Null;
	
	public static EnumClasses getClasseFromString(String s)
	{
		if(s.equalsIgnoreCase(Policier.toString()))
		{
			return Policier;
		}
		else if(s.equalsIgnoreCase(Rebelle.toString()))
		{
			return Rebelle;
		}
		else if(s.equalsIgnoreCase(Civil.toString()))
		{
			return Civil;
		}
		else if(s.equalsIgnoreCase(Null.toString()))
		{
			return Null;
		}
		else
		{
			return null;
		}
	}
}
