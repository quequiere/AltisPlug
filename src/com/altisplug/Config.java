package com.altisplug;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

import com.altisplug.Infraction.AutoDisapearStarThread;
import com.altisplug.Infraction.LimitePayDateThread;
import com.altisplug.bracage.Caisse;
import com.altisplug.hopital.Hopital;
import com.altisplug.network.SocketListener;
import com.altisplug.util.ClassLoaderQuequiere;
import com.altisplug.util.qqcolor;
import com.earth2me.essentials.Essentials;
import com.sun.org.apache.xml.internal.security.utils.JavaUtils;

public class Config
{

	public static FileConfiguration config;

	public static String user, pass, table, ip;
	public static int hospitalCost, hospitalTimeToPay;


	public static void Init(FileConfiguration fileConfiguration, AltisPlug plug)
	{
		System.out.println(qqcolor.ANSI_GREEN+"------Loading configuration-------"+qqcolor.ANSI_RESET);

		config = fileConfiguration;
		config.options().header("Fichier de configuration pour le plugin AltisPlug par quequiere et Sancho");

		// exemple d'ecriture d'une congi
		config.addDefault("general.sql.user", "user");
		config.addDefault("general.sql.pass", "pass");
		config.addDefault("general.sql.table", "table");
		config.addDefault("general.sql.ip", "ip");

		config.addDefault("play.hospital.cost", 2000);
		config.addDefault("play.hospital.timeToPay(min)", 300);
		
		config.addDefault("play.hospital.list", new ArrayList<String>());
		config.addDefault("play.caisse.list", new ArrayList<String>());
		
		

		//---------------------------------------------
		// sql
		user = config.getString("general.sql.user");
		pass = config.getString("general.sql.pass");
		table = config.getString("general.sql.table");
		ip = config.getString("general.sql.ip");

		// ingame setting
		hospitalCost = config.getInt("play.hospital.cost");
		hospitalTimeToPay = config.getInt("play.hospital.timeToPay(min)");
		Hopital.loadAll(config.getStringList("play.hospital.list"));
		Caisse.loadAll(config.getStringList("play.caisse.list"));
		

		config.options().copyDefaults(true);
		plug.saveConfig();

		// initialisation DB

		Database.LoadSql();

		System.out.println(qqcolor.ANSI_GREEN+"------Fin loading configuration ET DB -------"+qqcolor.ANSI_RESET);

		System.out.println(qqcolor.ANSI_GREEN+"------Debut de chargement des libs -------"+qqcolor.ANSI_RESET);

		boolean eco = plug.setupEconomy();
		boolean perm = plug.setupPermissions();
		System.out.println("Vaul result: " + eco + " / " + perm);

		// ------------------------------------
		Essentials essp = (Essentials) plug.getServer().getPluginManager().getPlugin("Essentials");
		if (essp != null)
		{
			System.out.println(qqcolor.ANSI_GREEN+"------Initialisation essential OK -------"+qqcolor.ANSI_RESET);
			AltisPlug.essentials = essp;
		}
		else
		{
			System.out.println(qqcolor.ANSI_RED+"!!!!!!!!!!! Essentials non valide !!!!!!!!!"+qqcolor.ANSI_RESET);
		}

		// ---------------------------------------

		System.out.println("Chargement des libs externes ( gson etc ...)");
		File s = new File("libs/gson.jar");
		try
		{
			ClassLoaderQuequiere.addClassPath(ClassLoaderQuequiere.getJarUrl(s));
		}
		catch (IOException e)
		{
			System.out.println(qqcolor.ANSI_RED+"Chargement error !!!!"+qqcolor.ANSI_RESET);
			e.printStackTrace();
		}

		System.out.println(qqcolor.ANSI_GREEN+"------Fin de chargement des libs -------"+qqcolor.ANSI_RESET);

		// --------------------------------

		System.out.println(qqcolor.ANSI_GREEN+"------Debut lancement des Threads -------"+qqcolor.ANSI_RESET);

		// ---------------
		LimitePayDateThread lpt = new LimitePayDateThread();
		lpt.start();
		//-----------------
		AutoDisapearStarThread adt = new AutoDisapearStarThread();
		adt.start();
		// ---------------
		SocketListener sl = new SocketListener(9989);
		Thread tsl = new Thread(sl);
		tsl.start();
		// ---------------

		System.out.println(qqcolor.ANSI_GREEN+"------Fin de lancement des Threads -------"+qqcolor.ANSI_RESET);

	}
}
