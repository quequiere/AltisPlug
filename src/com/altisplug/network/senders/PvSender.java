package com.altisplug.network.senders;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.altisplug.Infraction.Infraction;
import com.altisplug.classes.superClasse.Joueur;
import com.altisplug.classes.superClasse.JoueurPunissable;

public class PvSender
{
	public static void go(PrintWriter out, BufferedReader in, String target)
	{
		Joueur j = Joueur.getJoueur(target);

		if (j instanceof JoueurPunissable)
		{
			try
			{
				
				String name = in.readLine();
				
				Joueur sender = Joueur.getJoueur(name);
				JoueurPunissable jp = (JoueurPunissable) j;

				int nb = Integer.parseInt(in.readLine());
				for (int x = 0; x < nb; x++)
				{
					int id = Integer.parseInt(in.readLine());
					int montant = Integer.parseInt(in.readLine());
					int minutes = Integer.parseInt(in.readLine());

					Infraction i = jp.getInfractionById(id);
					if (i != null)
					{
						jp.getPlayer().sendMessage(ChatColor.BLUE + "Un policier a validé une de vos amendes pour " + montant + " $");
						sender.getPlayer().sendMessage(ChatColor.GREEN + "Vous avez valide une amende d'un montant suivant en tenant compte des limites max : " + montant + " $");
						i.setConstrol(sender.getPlayer().getDisplayName(), montant, minutes);

					}
					else
					{
						sender.getPlayer().sendMessage(ChatColor.RED + "Erreure dans la validation d'une amende: " + montant);
					}

				}
			}
			catch (NumberFormatException e)
			{

			}
			catch (IOException e)
			{
			}
		}
	}

}
