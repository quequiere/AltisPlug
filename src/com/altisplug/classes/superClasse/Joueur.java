package com.altisplug.classes.superClasse;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.altisplug.AltisPlug;
import com.altisplug.Database;
import com.altisplug.classes.Civil;
import com.altisplug.classes.Policer;
import com.altisplug.classes.Rebelle;

public class Joueur
{


	private static ArrayList<Joueur> LoadedJoueurs = new ArrayList<Joueur>();

	private UUID id;
	//private EnumClasses classe;
	private OfflinePlayer player;

	public Joueur(UUID id, OfflinePlayer player)
	{
		this.id = id;
		this.player = player;

		LoadedJoueurs.add(this);
	}

	public static Joueur loadPlayer(OfflinePlayer player)
	{
		System.out.println("-----Chargement des data pour " + player.getName() + "-----");

		Joueur j = null;

		try
		{
			Database.CheckSql();

			PreparedStatement stmt = null;
			stmt = Database.connect.prepareStatement("SELECT * FROM Joueurs WHERE `name` = ?");
			stmt.setString(1, player.getName());
			ResultSet rs = stmt.executeQuery();
			if (rs.next())
			{
				String classeName = rs.getString("classe");
				EnumClasses classe = EnumClasses.getClasseFromString(classeName);
				
				if(classe.equals(EnumClasses.Rebelle))
				{
					j = new Rebelle(player.getUniqueId(), player);
				}
				else if(classe.equals(EnumClasses.Policier))
				{
					j = new Policer(player.getUniqueId(), player);
				}
				else if(classe.equals(EnumClasses.Civil))
				{
					j = new Civil(player.getUniqueId(), player);
				}
				else
				{
					j = new Joueur(player.getUniqueId(), player);
				}

			

				System.out.println("-----Data chargees pour " + player.getName() + "-----"+j.getClasse().toString());
			}
			else
			{
				System.out.println("-----Creation d'un profil sql pour " + player.getName() + "-----");
				createDatabasePlayer(player);
				j = loadPlayer(player);
			}

			stmt.close();

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return j;

	}
	
	public void refreshFromDatabase()
	{
		try
		{
			Database.CheckSql();

			PreparedStatement stmt = null;
			stmt = Database.connect.prepareStatement("SELECT * FROM Joueurs WHERE `uuid` = ?");
			stmt.setString(1, this.id.toString());
			ResultSet rs = stmt.executeQuery();
			if (rs.next())
			{
				String classeName = rs.getString("classe");
				EnumClasses classe = EnumClasses.getClasseFromString(classeName);
				
				//gestion des erreures de changement de classe durant la deco
				
				if(classe.equals(EnumClasses.Policier)&&!(this instanceof Policer))
				{
					Bukkit.getPlayer(this.player.getUniqueId()).sendMessage(ChatColor.BLUE+"Changement de classe detectee en Policier ...Rechargement");
					LoadedJoueurs.remove(this);
					LoadedJoueurs.add(new Policer(this.player.getUniqueId(),this.player));
				}
				else if(classe.equals(EnumClasses.Rebelle)&&!(this instanceof Rebelle))
				{
					Bukkit.getPlayer(this.player.getUniqueId()).sendMessage(ChatColor.BLUE+"Changement de classe detectee en Rebelle ...Rechargement");
					LoadedJoueurs.remove(this);
					LoadedJoueurs.add(new Rebelle(this.player.getUniqueId(),this.player));
				}
				else
				{
					Bukkit.getPlayer(this.player.getUniqueId()).sendMessage(ChatColor.BLUE+"Rechargement de vos datas ok.");
				}
				
				//------------------------
				
				if(!(this instanceof Policer) && !(this instanceof Rebelle)&&!(this instanceof Civil))
				{

					Bukkit.getPlayer(this.player.getUniqueId()).sendMessage(ChatColor.RED+"Vous n'avez toujours pas choisi de classe ! /alt classe");
	
				}
				
			}
			else
			{
				System.out.println("Erreure critique dans le rechargement des datas joueurs, ligne sql certainement supprimee !");
			}

			stmt.close();

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	private static void createDatabasePlayer(OfflinePlayer player)
	{
		try
		{
			Database.CheckSql();
			PreparedStatement stmt = null;
			stmt = Database.connect.prepareStatement("INSERT INTO Joueurs (`name`,`uuid`,`classe`) VALUES(?,?,?)");
			stmt.setString(1, player.getName());
			stmt.setString(2, player.getUniqueId().toString());
			stmt.setString(3, "Null");

			stmt.executeUpdate();
			stmt.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public static Joueur getJoueur(String s)
	{
		for (Joueur j : LoadedJoueurs)
		{
			if (j.player.getName().equals(s))
			{
				return j;
			}
		}

		return null;
	}
	
	public boolean isRebel()
	{
		if(this instanceof Rebelle)
		{
			return true;
		}
		return false;
	}
	
	public boolean isPolicer()
	{
		if(this instanceof Policer)
		{
			return true;
		}
		return false;
	}
	
	public boolean isClasseNul()
	{
		if(!(this instanceof Policer) && !(this instanceof Rebelle))
		{
			return true;
		}
		return false;
	}

	public Player getPlayer()
	{
		return Bukkit.getPlayer(this.id);
	}

	public double moneyGet()
	{
		return AltisPlug.economy.getBalance(this.player);
	}

	public void moneyGive(double d)
	{
		AltisPlug.economy.depositPlayer(this.player, d);
	}

	public void moneyTake(double d)
	{
		AltisPlug.economy.withdrawPlayer(this.player, d);
	}
	
	public EnumClasses getClasse()
	{
		if(this instanceof Policer)
		{
			return EnumClasses.Policier;
		}
		else if(this instanceof Rebelle)
		{
			return EnumClasses.Rebelle;
		}
		else if(this instanceof Civil)
		{
			return EnumClasses.Civil;
		}
		else
		{
			return EnumClasses.Null;
		}
	}

	public void setClasse(EnumClasses classe)
	{
		// EDBUT FAIRE LENREGISTREMENT SQL !!!!
		LoadedJoueurs.remove(this);
		Joueur j=null;
		
		if(classe.equals(EnumClasses.Civil))
		{
			j=new Civil(this.player.getUniqueId(),this.player);
			LoadedJoueurs.add(j);
		}
		else if(classe.equals(EnumClasses.Rebelle))
		{
			j=new Rebelle(this.player.getUniqueId(),this.player);
			LoadedJoueurs.add(j);
		}
		else if(classe.equals(EnumClasses.Policier))
		{
			j=new Policer(this.player.getUniqueId(),this.player);
			LoadedJoueurs.add(j);
		}
		
		
		try
		{
			Database.CheckSql();
			PreparedStatement stmt = null;
			stmt = Database.connect.prepareStatement("UPDATE Joueurs SET `classe` = ? WHERE uuid = ?");
			stmt.setString(1, j.getClasse().toString());
			stmt.setString(2, player.getUniqueId().toString());

			stmt.executeUpdate();
			stmt.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		
		
		
		
		if(this.player.isOnline())
		{
			((Player) this.player).sendMessage(ChatColor.BLUE+"Votre classe a changee en: "+j.getClasse().toString());
		}
	}

}
