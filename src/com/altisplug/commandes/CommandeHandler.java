package com.altisplug.commandes;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.altisplug.AltisPlug;

public class CommandeHandler implements CommandExecutor
{
	public CommandeHandler()
	{

	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String main, String[] args)
	{
		if (cmd.getName().equalsIgnoreCase("altis"))
		{
			if (args.length == 0)
			{
				sender.sendMessage(ChatColor.RED + "Listes des commandes:");
				sender.sendMessage(ChatColor.BLUE + "/altis classe");
				sender.sendMessage(ChatColor.BLUE + "/altis infraction");
			}
			else if (!AltisPlug.permission.has(sender, "altisplug.commande." + args[0]))
			{
				// on check dynamiquement que le joueur a la perm
				sender.sendMessage(ChatColor.RED + "Vous ne disposez pas de cette commande ==> " + args[0]);
			}
			else if (args[0].equals("classe"))
			{
				ClasseCommande.execute(sender, args);
			}
			else if (args[0].equals("infraction"))
			{
				InfractionCommande.execute(sender, args);
			}
			else if (args[0].equals("addhopital"))
			{
				addHopital.execute(sender, args);
			}
			else if (args[0].equals("caisse"))
			{
				caisseCommande.execute(sender, args);
			}
		}
		return true;
	}

}
