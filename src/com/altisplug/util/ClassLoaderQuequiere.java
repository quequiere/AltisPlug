package com.altisplug.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

public class ClassLoaderQuequiere
{

	public static void addClassPath(final URL url) throws IOException
	{
		final URLClassLoader sysloader = (URLClassLoader) ClassLoader.getSystemClassLoader();
		final Class<URLClassLoader> sysclass = URLClassLoader.class;
		try
		{
			final Method method = sysclass.getDeclaredMethod("addURL", new Class[] { URL.class });
			method.setAccessible(true);
			method.invoke(sysloader, new Object[] { url });
		}
		catch (final Throwable t)
		{
			t.printStackTrace();
			throw new IOException("Error adding " + url + " to system classloader");
		}
	}

	public static URL getJarUrl(final File file) throws IOException
	{
		return new URL("jar:" + file.toURI().toURL().toExternalForm() + "!/");
	}

}
