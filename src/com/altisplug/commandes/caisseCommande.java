package com.altisplug.commandes;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.altisplug.bracage.Caisse;
import com.altisplug.hopital.Hopital;

public class caisseCommande
{
	public static void execute(CommandSender sender, String[] args)
	{
		if (sender instanceof Player)
		{
			Player p = (Player) sender;
			Caisse c = Caisse.getNearestCaisse(((Player) sender).getLocation());

			if (c != null)
			{
				if (args.length == 3)
				{
					try
					{
						int max = Integer.parseInt(args[1]);
						int current = Integer.parseInt(args[2]);
						
						if(!(current>max))
						{
							c.setMoneyMax(max);
							c.setCurrentMoney(current);
							
							sender.sendMessage(ChatColor.GREEN + "Votre caisse a correctement ete modifie !");
							sender.sendMessage(ChatColor.GREEN + "Le débit par seconde sera: "+Math.round(c.getFrequence()));
						}
						else
						{
							sender.sendMessage(ChatColor.RED + "Le montant courrant ne peut dépasser le montant max idiot !");
						}
						
					}
					catch (NumberFormatException e)
					{
						sender.sendMessage(ChatColor.RED + "Vous devez donner des chiffres !");
						return;
					}

				}
				else if(args.length == 2 && args[1].equalsIgnoreCase("info"))
				{
					sender.sendMessage(ChatColor.BLUE + "Max:"+c.getMoneyMax()+" Current:"+c.getCurrentMoney()+" Debit:"+c.getFrequence()+" en "+Caisse.totalTimeToBraquageSeconds+" secondes!");
				}
				else
				{
					sender.sendMessage(ChatColor.RED + "Usage : /alt caisse [max] [currentmoney] ");
					sender.sendMessage(ChatColor.RED + "Usage : /alt caisse info ");
				}
			}
			else
			{
				sender.sendMessage(ChatColor.RED + "Impossible de trouver une caisse a proximite !");
			}

		}
		else
		{
			sender.sendMessage(ChatColor.RED + "Impossible de faire cela depuis la console !");
			return;
		}

	}
}
