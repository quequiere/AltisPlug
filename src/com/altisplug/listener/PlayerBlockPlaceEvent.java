package com.altisplug.listener;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockPlaceEvent;

import com.altisplug.AltisPlug;
import com.altisplug.bracage.Caisse;
import com.altisplug.classes.superClasse.Joueur;

public class PlayerBlockPlaceEvent
{
	public static void go(BlockPlaceEvent e, Joueur j, Player p)
	{
		String type = e.getBlock().getType().toString();
		//debug
		
		if(type.equalsIgnoreCase("ALTISMOD_BLOCKCAISSE")||true)
		{
			if(AltisPlug.permission.has(p, "altisplug.block.caisse"))
			{
				Location loc = e.getBlock().getLocation();
				new Caisse(loc);
				p.sendMessage(ChatColor.GREEN+"Votre caisse a été créée. Vous pouvez la configurer avec /alt caisse");
			}
			else
			{
				p.sendMessage(ChatColor.RED+"Vous devez disposer de: altisplug.block.caisse");
				e.setCancelled(true);
			}
		}
		
	}

}
