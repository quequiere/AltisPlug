package com.altisplug.network.senders;

import java.io.PrintWriter;

import com.altisplug.classes.superClasse.Joueur;
import com.altisplug.classes.superClasse.JoueurPunissable;

public class OpenDossier
{
	public static void go(PrintWriter out, String target)
	{
		Joueur j = Joueur.getJoueur(target);
		
		if(j instanceof JoueurPunissable)
		{
			JoueurPunissable jp = (JoueurPunissable) j;
			jp.setFreeze();
			
		}
	}

}
