package com.altisplug;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.altisplug.util.qqcolor;


public class Database
{
	
	public static String jdbc = "jdbc:mysql://"+Config.ip+"/"+Config.table+"?user="+Config.user+"&password="+Config.pass;
	
	public static Connection connect = null;
	
	public static void CheckSql()
	{
		try
		{
			if (connect == null || connect.isClosed()) 
			{
				connect = DriverManager.getConnection(jdbc);
			}
		}
		catch (SQLException e)
		{
			System.out.println("SQL check problem: "+e);
		}
	}
	
	public static void LoadSql()
	{
		System.out.println(qqcolor.ANSI_GREEN+"---------Load de la DB ----------"+qqcolor.ANSI_RESET);
		
		try
		{
			connect = DriverManager.getConnection(jdbc);
			if(connect.isValid(1))
			{
				System.out.println("La connection SQL est ok !");
			}
			else
			{
				System.out.println(qqcolor.ANSI_RED+"------Connection SQL echouee !-------"+qqcolor.ANSI_RESET);
			}
		}
		catch (SQLException e)
		{
			System.out.println("SQL check problem: "+e);
		}
	}
}
