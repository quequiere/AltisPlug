package com.altisplug.listener;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.altisplug.classes.Policer;
import com.altisplug.classes.Rebelle;
import com.altisplug.classes.superClasse.EnumClasses;
import com.altisplug.classes.superClasse.Joueur;
import com.altisplug.hopital.Hopital;

public class PlayerListener implements Listener
{

	public static Server server = null;

	public PlayerListener(Server server)
	{
		PlayerListener.server = server;
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void LoginEvent(org.bukkit.event.player.PlayerJoinEvent e)
	{
		// ------------------------------- INITIALISATION
		Player p = e.getPlayer();
		System.out.println("-----" + p.getName() + " rejoins le serveur ----");

		Joueur j = Joueur.getJoueur(p.getName());

		if (j != null)
		{
			System.out.println("-----Rafraichissement des datas Joueur pour " + p.getName() + "----");
			j.refreshFromDatabase();
			System.out.println("-----Fin du raffraichissement Joueur pour " + p.getName() + "----");
			
			if(j instanceof Policer)
			{
				p.sendMessage(ChatColor.BLUE+"Vous rejoignez le jeu en tant que Policier!");
			}
			else if(j instanceof Rebelle)
			{
				p.sendMessage(ChatColor.BLUE+"Vous rejoignez le jeu en tant que Rebelle!");
			}
			else
			{
				p.sendMessage(ChatColor.BLUE+"Vous rejoignez le jeu en tant que --Sans classe--");
			}
			
		}
		else
		{
			System.out.println("-----Chargement du profil Joueur pour " + p.getName() + "----");
			j = Joueur.loadPlayer(p);

			if (j == null)
			{
				System.out.println("-----Erreure critique de chargement pour " + p.getName() + "----");
			}

		}

		// ------------------------------- FIN D INITIALISATION

		PlayerLoadEvent.LoginPlayer(e, p, j);

		// ON n'ajoute rien ici, tous l'event se passe dans LoginPlayer qui
		// dispose de tous les bon objets.

	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void MooveEvent(org.bukkit.event.player.PlayerMoveEvent e)
	{
		// ON n'ajoute rien ici, tous l'event se passe dans mooveEvent

		Player p = e.getPlayer();
		Joueur j = Joueur.getJoueur(p.getName());
		
		Location oldLoc = e.getFrom();
		
		if(j.isClasseNul())
		{
			p.teleport(oldLoc);
			p.sendMessage(ChatColor.RED+"Vous ne pouvez pas jouer, le temps que vous n'avez pas de classe!");
			p.sendMessage(ChatColor.BLUE+"Faire: /alt classe rebelle/policier/civil");
			return;
			
			//on return directement, aucune autre chose ne peut être faite !
		}

		//Coder dans cette fonction uniquement
		PlayerMooveEvent.mooveEvent(e, j, p);

	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void DieEventPlayer(PlayerDeathEvent e)
	{	
		Player p = e.getEntity();
		Joueur j = Joueur.getJoueur(p.getName());
		PlayerDieEvent.dieEvent(e,j,p);
		
	}
	
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void BlockPlayer(BlockPlaceEvent e)
	{	
		Player p = e.getPlayer();
		Joueur j = Joueur.getJoueur(p.getName());
		PlayerBlockPlaceEvent.go(e,j,p);
		
	}
	
	
	
	
	 

}
