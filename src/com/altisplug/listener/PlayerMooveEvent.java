package com.altisplug.listener;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;

import com.altisplug.classes.superClasse.Joueur;
import com.altisplug.classes.superClasse.JoueurPunissable;

public class PlayerMooveEvent
{
	public static void mooveEvent(PlayerMoveEvent e, Joueur j, Player p)
	{
		if(j instanceof JoueurPunissable)
		{
			JoueurPunissable jp = (JoueurPunissable) j;
			
			if(jp.isFreeze())
			{
				p.sendMessage(ChatColor.RED+"Un policier vous contrôle, vous ne pouvez pas bouger pour le moment.");
				p.teleport(e.getFrom());
			}
		}
	}

}
